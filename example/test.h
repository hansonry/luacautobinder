#ifndef __TEST_H__
#define __TEST_H__

struct vector
{
   int x;
   int y;
   int z;
};

union xform
{
   int i;
   float f;
};

enum result
{
   e_R_success,
   e_R_failure
};

#define SOMETHING 1

extern int TheGlobal;

int 
test1(void);

void test2(int a);

void test3(int a,
           int b);

const char * convert(const char * str);

#endif // __TEST_H__



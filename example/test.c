#include <stdio.h>
#include "test.h"

int test1(void)
{
   printf("Test1 Function\n");
   return 10;
}


void test2(int a)
{
   printf("Test2 Function: A=%d\n", a);
}

void test3(int a,
           int b)
{
   printf("Test3 Function: A=%d B=%d\n", a, b);
}

const char * convert(const char * str)
{
   printf("convert Function: str=%s\n", str);
   return "HI MOM!";
}

int TheGlobal = 5;

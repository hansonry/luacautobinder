

function RunCommand(cmd)
   local f = assert(io.popen(cmd, 'r'))
   local s = assert(f:read('*a'))
   f:close()
   return s
end


function split(text, splitCharacter)
   local splitArray = {}
   for s in string.gmatch(text, "[^" .. splitCharacter .. "]+") do
      table.insert(splitArray, s)
   end
   return splitArray
end


function RunTags(filename)
   function createInfo(cols)
      function parseParameter(param)
         local lastSpace = string.find(param, " [^ ]*$")
         return { 
            type = string.sub(param, 1, lastSpace - 1),
            name = string.sub(param, lastSpace + 1)
         }
      end
      local info = {
         name       = cols[1],
         sourcefile = cols[2]
      }
      local t = cols[4]
      if t == "d" then
         info.type = "constant"
      elseif t == "x" then
         info.type = "variable"
      elseif t == "m" then
         info.type = "structMember"
      elseif t == "e" then
         info.type = "enumMember"
      elseif t == "g" then
         info.type = "enum"
      elseif t == "p" then
         info.type = "function"
      elseif t == "s" then
         info.type = "struct"
      elseif t == "u" then
         info.type = "union"
      else
         info.type = "unknown"
      end

      if info.type == "structMember" then
         local parent = split(cols[5], ":")
         assert(parent[1] == "enum" or 
                parent[1] == "struct" or
                parent[1] == "union")
         info.structType = parent[1]
         info.structName = parent[2]
      end


      if info.type == "function" then
         local rettype = split(cols[5], ":")
         assert(rettype[1] == "typeref")
         assert(rettype[2] == "typename")
         info.returnType = rettype[3]
         info.parameters = {}
         local signature = split(cols[6], ":")
         assert(signature[1] == "signature")
         local params = string.sub(signature[2], 2, -2)
         for param in string.gmatch(params, "[^,]+") do
            if param ~= "void" then
               table.insert(info.parameters, parseParameter(param))
            end
         end
      end

      return info
   end
   local cmd = string.format("uctags --options=ctagsOptions.txt %s", filename)
   local output = RunCommand(cmd)
   print(output)
   local tags = {}
   for line in string.gmatch(output, '[^\r\n]+') do
     local cols = split(line, "\t")
     tags[cols[1]] = createInfo(cols)
   end
   return tags
end

function typeNameToCheckFunction(tags, ctype)
   if ctype == "int" or 
      ctype == "long" or
      ctype == "char" or
      ctype == "unsigned int" or
      ctype == "unsigned long" or
      ctype == "unsigned char" then
      return "luaL_checkinteger"
   end
   if ctype == "double" or
      ctype == "float" then
      return "luaL_checknumber"
   end
   if ctype == "const char *" then
      return "luaL_checkstring"
   end
   assert(ctype ~= "char *", "This library does not support editable strings")
   assert(false, "No idea what type we have:".. ctype)
end

function typeNameToPushCode(tags, ctype)
   if ctype == "int" or 
      ctype == "long" or
      ctype == "char" or
      ctype == "unsigned int" or
      ctype == "unsigned long" or
      ctype == "unsigned char" then
      return "lua_pushinteger"
   end
   if ctype == "double" or
      ctype == "float" then
      return "lua_pushnumber"
   end
   if ctype == "char *" or
      ctype == "const char *" then
      return "lua_pushstring"
   end
   assert(false, "No idea what type we have:".. ctype)
end

function writeWrapperFunction(f, tags, ftag)
   local returnCount
   f:write('static int lwrapper_', ftag.name, '(lua_State *L)\n')
   f:write('{\n')
   for i, param in ipairs(ftag.parameters) do
      f:write('   ', param.type, ' ', param.name, ' = ')
      f:write(typeNameToCheckFunction(tags, param.type))
      f:write('(L, ', i, ');\n')
   end
      f:write('   ')
   if ftag.returnType ~= "void" then
      f:write(ftag.returnType, ' l___result = ') 
   end
   f:write(ftag.name, '(')
   for i, param in ipairs(ftag.parameters) do
      if i ~= 1 then
         f:write(', ')
      end
      f:write(param.name)
   end
   f:write(');\n')

   if ftag.returnType == "void" then
      returnCount = 0
   else
      returnCount = 1
      f:write('   ', typeNameToPushCode(tyags, ftag.returnType))
      f:write('(L, l___result);\n')
   
   end
   f:write('   return ', returnCount, ';\n')
   f:write('}\n')
   f:write('\n')
end

local tags = RunTags("example/test.h")

local moduleName = "test"
local sourceFileName = moduleName .. ".c"
local f = assert(io.open(sourceFileName, "w"))

f:write('#include <lua.h>\n')
f:write('#include <lauxlib.h>\n')
f:write('\n')
f:write('#include <', moduleName, '.h>\n')
f:write('\n')
f:write('// Wrapper Functions\n')
for k, v in pairs(tags) do
   if v.type == "function" then
      writeWrapperFunction(f, tags, v)
   end
end
f:write('// Function Table\n')
f:write('static const struct luaL_Reg libFunctions[] = {\n')
for k, v in pairs(tags) do
   if v.type == "function" then
      f:write('   { "', v.name, '", lwrapper_', v.name, ' },\n')
   end
end
f:write('   { NULL, NULL }\n')
f:write('};\n')
f:write('\n')
f:write('int luaopen_', moduleName, '(lua_State *L)\n')
f:write('{\n')
f:write('   luaL_newlib(L, libFunctions);\n')
f:write('   return 1;\n')
f:write('}\n')
f:write('\n')

f:close()


